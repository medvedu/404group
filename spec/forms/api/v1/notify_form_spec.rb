require 'rails_helper'

module Api::V1
  describe NotifyForm, type: :model do
    context 'when request pass through notify form' do
      before :all do
        package = Package.create(password: '12345,', from: 'kate', to: 'bill')

        Delivery.create(package: package,
                        deliver_at: 2.minutes.ago,
                        delivered: true,
                        name: :telegram,
                        raw_text: 'very special message')
      end

      before :each do
        allow_any_instance_of(NotifyForm)
          .to receive(:call_notification_service_object).and_return(nil)
      end

      let :form_params do
        { viber: { from: 'km', password: '221100', to: 'Паша' },
          telegram: { from: 'robo', password: '334455', to: 'Виктор' },
          whats_app: { from: 'Loki', to: '@PAIN', password: '770011' },
          deliver_at: 10.days.from_now.utc,
          message: 'my super message' }
      end

      it 'perform attributes validations' do
        form = NotifyForm.new form_params
        expect(form.perform).to be_truthy
      end

      it 'returns an error when messenger is not present' do
        form_params[:viber] = nil
        form_params[:telegram] = nil
        form_params[:whats_app] = nil

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .to include(/Messenger not present/)
      end

      it 'returns an error when messenger is not valid' do
        form_params[:viber][:password] = nil

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .to include(/All attributes must be present:/)
      end

      it 'returns an error when date is not valid' do
        form_params[:deliver_at] = 'jokki'

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .to include(/is not a valid time/)
      end

      it 'returns an error when message too short' do
        form_params[:message] = 'j'

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .to include(/too short/)
      end

      it 'return an error when message was already sent in last 5 mins' do
        form_params[:message] = 'very special message'
        form_params[:deliver_at] = nil
        form_params[:telegram][:to] = 'bill'

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .to include(/Message not unique/)
      end

      it 'respects +deliver_at+ param for message with repeatable content' do
        form_params[:message] = 'very special message'
        form_params[:deliver_at] = 6.minutes.from_now
        form_params[:telegram][:to] = 'bill'

        form = NotifyForm.new form_params
        form.perform

        expect(form.errors.full_messages)
          .not_to include(/Message not unique/)
      end
    end
  end
end
