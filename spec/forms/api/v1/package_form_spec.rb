require 'rails_helper'

module Api::V1
  describe PackageForm, type: :model do
    context 'when request pass through message form' do
      let :params do
        { from: 'bill', to: 'kale', password: '12345' }
      end

      it 'returns an error when any attribute is blank' do
        params[:to] = nil

        errors = PackageForm.errors? params

        expect(errors.full_messages)
          .to include(/All attributes must be present/)
      end

      it 'returns no errors when all attributes are valid' do
        errors = PackageForm.errors? params
        expect(errors).to be_falsey
      end
    end
  end
end
