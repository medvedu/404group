require 'rails_helper'

RSpec.describe Emulator do
  let :params do
    [
      'message',
      :telegram,
      { from: 'kate', to: 'bill', password: '1234' },
      {}
    ]
  end

  it 'simply redirects a message to emulator' do
    expect_any_instance_of(Emulator::Telegram)
      .to receive(:notify)

    described_class.emulate! *params
  end
end
