require 'rails_helper'

module Api::V1
  describe StartEmulationJob do
    let :params do
      ['', '', {}, {}, 2]
    end

    it 'tries to emulate a request a few time in a row' do
      expect(Emulator).to receive(:emulate!)
      expect(Emulator).to receive(:emulate!)

      described_class.new.perform(*params)
    end
  end
end
