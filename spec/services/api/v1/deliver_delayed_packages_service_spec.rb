require 'rails_helper'

module Api::V1
  describe DeliverDelayedPackagesService do
    before :all do
      package = Package.create(password: '12345,', from: 'ccc', to: 'bbb')

      3.times do |i|
        Delivery.create(package: package,
                        deliver_at: (i * 3).minutes.ago,
                        delivered: false,
                        name: %i[whats_app telegram].sample,
                        raw_text: "message ##{i}")
      end
    end

    before :each do
      allow(Emulator).to receive(:emulate!).and_return(:success)
    end

    it 'delivers delayed packages' do
      expect(Delivery.where(delivered: false).count).to eq(3)

      described_class.new.perform

      expect(Delivery.where(delivered: false).count).to eq(0)
    end
  end
end
