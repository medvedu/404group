require 'rails_helper'

#####################
# INTEGRATION TESTS #
#####################

RSpec.describe 'Notifications API v1', type: :request do
  # NotificationsController#create
  # POST api/v1/notifications
  context 'when someone tries to send a notification' do
    # See README.MD (Section 'Заметки', №4)
    # before :each do
    #   [Emulator::Telegram, Emulator::Viber, Emulator::WhatsApp].each do |emulator|
    #     allow_any_instance_of(emulator)
    #       .to receive(:call_internal_logic).and_return(nil)
    #   end
    # end

    let :headers do
      { 'Content-Type' => 'application/json' }
    end

    let :body_delayed do
      {
        viber: { from: 'zzz', password: '11222', to: 'yyy' },
        telegram: { from: 'zzz', password: '67890', to: 'yyy' },
        whats_app: { from: 'zzz', to: 'yyy', password: 'ssssss' },
        message: 'test',
        deliver_at: Time.now.utc + 15
      }.to_json
    end

    let :body_instant do
      {
        viber: { from: 'zzz', password: '11222', to: 'yyy' },
        telegram: { from: 'zzz', password: '67890', to: 'yyy' },
        whats_app: { from: 'zzz', to: 'yyy', password: 'ssssss' },
        message: 'test'
      }.to_json
    end

    it 'creates a new notification' do
      post '/api/v1/notifications', params: body_delayed, headers: headers

      expect(response).to have_http_status(:ok)
      expect(response.content_type).to eq('application/json')
      expect(response.body)
        .to include('Emulation service accept your request')
    end

    it 'instantly sends a message when +deliver_at+ is null' do
      expect_any_instance_of(Emulator::Telegram).to receive(:notify)

      post '/api/v1/notifications', params: body_instant, headers: headers
    end

    it 'can send a message to several emulators at same time' do
      expect_any_instance_of(Emulator::Telegram).to receive(:notify)
      expect_any_instance_of(Emulator::Viber).to receive(:notify)
      expect_any_instance_of(Emulator::WhatsApp).to receive(:notify)

      post '/api/v1/notifications', params: body_instant, headers: headers
    end

    it 'spawns async delivery job if first deliver was unsuccessful' do
      body = {
        viber: { from: 'zzz', password: '11222', to: 'yyy' },
        whats_app: { from: 'zzz', to: 'yyy', password: 'ssssss' },
        message: 'test'
      }.to_json

      expect_any_instance_of(Emulator::Viber)
        .to receive(:notify).and_return(:fail)

      expect_any_instance_of(Emulator::WhatsApp)
        .to receive(:notify).and_return(:fail)

      expect(Api::V1::StartEmulationJob).to receive(:perform_later)
      expect(Api::V1::StartEmulationJob).to receive(:perform_later)

      post '/api/v1/notifications', params: body, headers: headers
    end

    it 'does not instantly sends a message when +deliver_at+ is present' do
      expect_any_instance_of(Emulator::Telegram).not_to receive(:notify)
      expect_any_instance_of(Emulator::Viber).not_to receive(:notify)
      expect_any_instance_of(Emulator::WhatsApp).not_to receive(:notify)

      post '/api/v1/notifications', params: body_delayed, headers: headers
    end

    it 'returns an error when +deliver_at+ is not valid time' do
      body = {
        viber: { from: 'zzz', password: '11222', to: 'yyy' },
        deliver_at: 'asdf',
        message: 'hi'
      }.to_json

      post '/api/v1/notifications', params: body, headers: headers

      expect(response).to have_http_status(:bad_request)
      expect(response.body).to include('errors')
    end

    it 'returns an error when same notification already been sent' do
      post '/api/v1/notifications', params: body_instant, headers: headers
      post '/api/v1/notifications', params: body_instant, headers: headers

      expect(response).to have_http_status(:bad_request)
      expect(response.body).to include('not_unique_message')
    end
  end
end
