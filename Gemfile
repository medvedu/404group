source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

#############
# CORE GEMS #
#############

gem 'pg',                               '>= 0.18', '< 2.0'
gem 'puma',                             '~> 3.11'
gem 'rails',                            '~> 5.2.2'

##############
# OTHER GEMS #
##############

gem 'validates_timeliness',             '~> 5.0.0.alpha3'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap',                         '>= 1.1.0', require: false

# A scheduler process to replace cron.
gem 'clockwork',                        '~> 2.0.3'

#########################
#   DEVELOPMENT & TEST  #
#########################

group :test do
  # Database Cleaner is a set of strategies for cleaning your database in Ruby.
  gem 'database_cleaner', require: false

  # Rspec
  gem 'rspec-rails', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rb-readline'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Code quality
  gem 'fasterer', require: false
  gem 'reek', require: false
  gem 'rubocop', require: false
  gem 'rubocop-rspec', require: false
end

group :development, :test do
  # Git hook manager
  gem 'overcommit', require: false

  # Application safety
  gem 'brakeman', require: false
end
