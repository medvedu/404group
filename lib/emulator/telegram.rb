class Emulator
  class Telegram
    # @param [String]
    #   sample => 'Hi, there!'
    #
    # @param [Hash] params
    #   sample => { "from"=>"kr@ska", "to"=>"Masha", "password"=>"11222" }
    #
    # @param [Hash] optional
    #   sample => { }
    #
    # @return [:success, :fail]
    def notify(message, params, optional = {})
      message_sent?(message, params, optional) ? :success : :fail
    end

    private

    # simple simulation...
    # # @return [TrueClass, FalseClass]
    def message_sent?(message, params, optional = {})
      call_internal_logic(message, params, optional)
    end

    # Sometimes telegram (or any other) server may not respond due to simulation requirement.
    def call_internal_logic(_message, _params, _optional = {})
      rand(100) > 55
    end
  end
end
