class Emulator
  class WhatsApp
    # @return [:success, :fail]
    def notify(message, params, optional = {})
      message_sent?(message, params, optional) ? :success : :fail
    end

    private

    # @return [TrueClass, FalseClass]
    def message_sent?(message, params, optional = {})
      call_internal_logic(message, params, optional)
    end

    def call_internal_logic(_message, _params, _optional = {})
      rand(100) > 15
    end
  end
end
