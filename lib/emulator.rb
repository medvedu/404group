class Emulator
  SERVICES_MAPPING = {
    viber: ::Emulator::Viber,
    telegram: ::Emulator::Telegram,
    whats_app: ::Emulator::WhatsApp
  }.freeze

  # @param [String] message
  # @param [Symbol] service
  # @param [Hash] data
  # @param [Hash] optional
  #
  # @return [Symbol<:success, :fail>]
  def self.emulate!(message, service, data, optional = {})
    klass = SERVICES_MAPPING.fetch(service, ::Emulator::Viber)
    klass.new.notify(message, data, optional)
  end
end
