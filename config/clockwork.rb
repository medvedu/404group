require 'clockwork'

require './config/boot'
require './config/environment'

require 'active_support/time'

module Clockwork
  every(2.minutes, 'Clockwork: deliver delayed packages') do
    ::Api::V1::DeliverDelayedPackagesService.new.perform
  end
end
