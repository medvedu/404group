class CreateDeliveries < ActiveRecord::Migration[5.2]
  def change
    create_table :deliveries do |t|
      t.time :deliver_at
      t.string :name
      t.boolean :delivered
      t.string :raw_text

      t.integer :package_id, index: true

      t.timestamps
    end
  end
end
