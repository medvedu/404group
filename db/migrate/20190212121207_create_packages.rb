class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :from
      t.string :to
      t.string :password

      t.timestamps
    end
  end
end
