module Api
  module V1
    class NotificationService
      # @param [String] message
      # @param [Hash<Symbol: Array>] services
      # @param [Hash] optional
      def initialize(message, services, optional = {})
        @message = message
        @services = services.map { |k, v| v.present? ? k : nil }.compact
        @data = services
        @optional = optional

        @deliver_at = optional.delete(:deliver_at)
        @deliver_at = deliver_at.to_time(:utc) unless deliver_at.blank?
      end

      def perform
        save_message

        spawn_emulation_or_add_to_async_job_if_fail unless deliver_at.present?
      end

      private

      # @return [String]
      attr_reader :message

      # @return [Array<Symbol>]
      attr_reader :services

      # @return [Hash]
      attr_reader :data

      # @return [Hash]
      attr_reader :optional

      # @return [Time, NilClass]
      attr_reader :deliver_at # UTC

      def save_message
        ActiveRecord::Base.transaction do
          @services.each do |service|
            from = data[service]['from']
            to = data[service]['to']
            password = data[service]['password']

            package = Package.create(password: password, from: from, to: to)
            Delivery.create(package: package, deliver_at: deliver_at, delivered: false,
                            name: service, raw_text: message)
          end
        end
      end

      def spawn_emulation_or_add_to_async_job_if_fail
        @services.each do |service|
          status = ::Emulator.emulate!(message, service, data[service], optional)

          next unless status == :fail

          retry_count = ENV['N'].to_i
          Api::V1::StartEmulationJob.perform_later(
            message, service.to_s, data[service], optional, retry_count
          )
        end
      end
    end
  end
end
