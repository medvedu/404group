module Api
  module V1
    class DeliverDelayedPackagesService
      def initialize; end

      def perform
        deliver
      end

      private

      def deliver
        delayed =
          Delivery.where('delivered = false AND deliver_at <= ?', Time.now.utc)
                  .includes(:package)

        delayed.each do |delivery|
          from = delivery.package.from
          to = delivery.package.to
          password = delivery.package.password

          status =
            ::Emulator.emulate!(delivery.raw_text, delivery.name,
                                from: from, to: to, password: password)

          delivery.delivered = true if status == :success
          delivery.save
        end
      end
    end
  end
end
