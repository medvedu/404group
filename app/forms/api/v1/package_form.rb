module Api
  module V1
    class PackageForm
      include ActiveModel::Model

      # @params [Hash] hsh
      #
      # @return [ActiveModel::Errors , FalseClass]
      def self.errors?(hsh)
        form = PackageForm.new(hsh)
        form.valid? ? false : form.errors
      end

      attr_accessor(
        :from,
        :password,
        :to
      )

      validate :package_valid?

      private

      def package_valid?
        if from.blank? || password.blank? || to.blank?
          errors.add(:package_invalid,
                     message: I18n.t('api.v1.form.package.param_missed'))
        end
      end
    end
  end
end
