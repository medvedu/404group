module Api
  module V1
    class NotifyForm
      include ActiveModel::Model

      attr_accessor(
        :viber,
        :telegram,
        :whats_app,
        :deliver_at,
        :message
      )

      validates_time :deliver_at,
                     allow_nil: true,
                     on_or_after: Time.now.utc

      validates :message, length: { minimum: 2, maximum: 8096 },
                          presence: true

      validate :each_messenger_valid_or_blank

      validate :at_least_one_messenger_is_present

      validate :message_is_not_repeatable_in_last_5_mins

      # @return [TrueClass, FalseClass]
      def perform
        return false unless valid?

        call_notification_service_object || true
      end

      private

      def call_notification_service_object
        services = { viber: viber, telegram: telegram, whats_app: whats_app }
        optional = { deliver_at: deliver_at }

        ::Api::V1::NotificationService.new(
          message, services, optional
        ).perform
      end

      def each_messenger_valid_or_blank
        [whats_app, telegram, viber].each do |messenger|
          next if messenger.blank?

          if msg_err = Api::V1::PackageForm.errors?(messenger)
            errors.merge! msg_err
          end
        end
      end

      def at_least_one_messenger_is_present
        error = [whats_app, telegram, viber].all? &:blank?

        if error
          errors.add(:messenger_not_present,
                     message: I18n.t('api.v1.form.notification.not_present'))
        end
      end

      # Delayed deliver forbidden too
      def message_is_not_repeatable_in_last_5_mins
        return if ::Delivery.count.zero? ||
                  (deliver_at.is_a?(Time) && deliver_at > Time.now.utc + 300)

        persons =
          begin
            deliveries =
              Delivery.select(:to)
                      .where('raw_text = ? AND deliveries.created_at >= ?', message, Time.now - 5.minutes)

            deliveries.includes(:package).pluck(:to)
          end

        [whats_app, telegram, viber].each do |messenger|
          next unless messenger

          if persons.any? { |p| p.include?(messenger[:to]) }
            errors.add(:not_unique_message,
                       message: I18n.t('api.v1.form.notification.not_unique_message'))
          end
        end
      end
    end
  end
end
