module Api
  module V1
    class StartEmulationJob < ApplicationJob
      queue_as :default

      def perform(message, service, data, optional, retry_count)
        _perform(message, service, data, optional, retry_count)
      end

      private

      def _perform(message, service, data, optional, retry_count)
        retry_count.times do
          status = ::Emulator.emulate!(message, service, data, optional)
          return if status == :success
        end
      end
    end
  end
end
