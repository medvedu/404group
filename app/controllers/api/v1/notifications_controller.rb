# frozen_string_literal: true

module Api
  module V1
    class NotificationsController < ApplicationController
      # POST api/v1/notifications
      def create
        form = ::Api::V1::NotifyForm.new(strong_params)

        if form.perform
          render json: { message: I18n.t('api.v1.controller.notification.accept') }, status: :ok
        else
          render json: { errors: form.errors.keys }, status: :bad_request
        end
      end

      private

      # @return [ActionController::Parameters]
      def strong_params
        params.require(:notification)
              .permit(:message, :deliver_at,
                      viber: %i[from to password],
                      telegram: %i[from to password],
                      whats_app: %i[from to password])
      end
    end
  end
end
